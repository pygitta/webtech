package pygitta.beadando01.domain;

import java.util.Date;

public class Sale {

    private String productId;

    private String customerId;

    private int piece;

    private double price;

    private Date date;

    public Sale(String productId, String customerId, int piece) {
        this.productId = productId;
        this.customerId = customerId;
        this.piece = piece;
    }

    public Sale(String productId, String customerId, int piece, double price, Date date) {
        this.productId = productId;
        this.customerId = customerId;
        this.piece = piece;
        this.price = price;
        this.date = date;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getPiece() {
        return piece;
    }

    public void setPiece(int piece) {
        this.piece = piece;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
