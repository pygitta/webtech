package pygitta.beadando01.dao;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;
import pygitta.beadando01.domain.Customer;
import pygitta.beadando01.domain.Product;
import pygitta.beadando01.domain.Sale;
import pygitta.beadando01.domain.Stock;
import pygitta.beadando01.exception.NoSuchProductException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class WebShopDao {

    private List<Product> products = Lists.newArrayList(
            new Product("ASD123", "Until Dawn", "Until Dawn is a video game developed by Supermassive Games"),
            new Product("ASD125", "Red Dead Redemption 2", "It is a video game developed by Rockstar Games")
    );

    private List<Stock> stockTable = Lists.newArrayList(
            new Stock("ASD123", 10, 43.50, new Date()),
            new Stock("ASD125", 14, 35.50, new Date())
    );

    private List<Customer> customers = Lists.newArrayList(
            new Customer("brigi", "Brigitta Baranyai", "3425 Budapest, Galagonya utca 23/a")
    );

    private List<Sale> saleTable = Lists.newArrayList();

    public List<Product> getProducts() {
        return products;
    }

    public Product getProduct(String productId) {
        Optional<Product> optionalProduct = products.stream()
                .filter(product -> product.getProductId().equals(productId))
                .findFirst();
        return optionalProduct.orElseThrow(NoSuchProductException::new);
    }

    public Stock getStockInformation(String productId) {
        Optional<Stock> optionalStock = stockTable.stream()
                .filter(stock -> stock.getProductId().equals(productId))
                .findFirst();
        return optionalStock.orElseThrow(NoSuchProductException::new);
    }

    public void insertNewSaleData(Sale saleInformation) {
        Stock stockInformation = getStockInformation(saleInformation.getProductId());
        double price = stockInformation.getPrice() * saleInformation.getPiece();
        saleTable.add(new Sale(saleInformation.getProductId(), saleInformation.getCustomerId(),
                saleInformation.getPiece(), price, new Date()));
        stockInformation.setPiece(stockInformation.getPiece() - 1);
    }

}
