package pygitta.beadando01.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import pygitta.beadando01.dao.WebShopDao;
import pygitta.beadando01.domain.Product;
import pygitta.beadando01.domain.Stock;

import java.util.List;

@Service
public class WebShopRestServiceImpl implements WebShopRestService {

    private WebShopDao webShopDao;

    @Autowired
    public WebShopRestServiceImpl(WebShopDao webShopDao) {
        this.webShopDao = webShopDao;
    }

    @Override
    public List<Product> getProducts() {
        return webShopDao.getProducts();
    }

    @Override
    public Product getProduct(String productId) {
        return webShopDao.getProduct(productId);
    }

    @Override
    public Stock getStockInformation(String productId) {
        return webShopDao.getStockInformation(productId);
    }
}
