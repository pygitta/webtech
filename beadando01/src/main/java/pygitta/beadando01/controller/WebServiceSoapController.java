package pygitta.beadando01.controller;

import com.gdpotter.sample.iso_20022.seev_031_01_07.SaleRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pygitta.beadando01.domain.Sale;
import pygitta.beadando01.service.WebShopSoapService;

@Endpoint
public class WebServiceSoapController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceSoapController.class);

    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private WebShopSoapService webShopSoapService;

    @Autowired
    public WebServiceSoapController(WebShopSoapService webShopSoapService) {
        this.webShopSoapService = webShopSoapService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "saleRequest")
    @ResponsePayload
    public void sellProduct(@RequestPayload SaleRequest request) {
        LOGGER.info("Sell request for product with id {}", request.getSaleInformation().getProductId());
        webShopSoapService.sellProduct(new Sale(request.getSaleInformation().getProductId(),
                request.getSaleInformation().getCustomerId(),
                request.getSaleInformation().getPiece()));
    }
}
