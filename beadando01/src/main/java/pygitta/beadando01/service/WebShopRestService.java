package pygitta.beadando01.service;

import pygitta.beadando01.domain.Product;
import pygitta.beadando01.domain.Stock;

import java.util.List;

public interface WebShopRestService {

    List<Product> getProducts();

    Product getProduct(String productId);

    Stock getStockInformation(String productId);
}
