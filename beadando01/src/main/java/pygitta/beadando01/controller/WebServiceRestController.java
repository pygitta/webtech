package pygitta.beadando01.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pygitta.beadando01.domain.Product;
import pygitta.beadando01.domain.Stock;
import pygitta.beadando01.service.WebShopRestService;

import java.util.List;

@RestController
@RequestMapping("/")
public class WebServiceRestController {

    private WebShopRestService webShopRestService;

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceRestController.class);

    @Autowired
    public WebServiceRestController(WebShopRestService webShopRestService) {
        this.webShopRestService = webShopRestService;
    }

    @RequestMapping(value = "product", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getProducts() {
        LOGGER.info("Fetching all products");
        return new ResponseEntity<>(webShopRestService.getProducts(), HttpStatus.OK);
    }

    @RequestMapping(value = "product/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> getProduct(@PathVariable("id") String productId) {
        LOGGER.info("Fetching product information with id {}", productId);
        return new ResponseEntity<>(webShopRestService.getProduct(productId), HttpStatus.OK);
    }

    @RequestMapping(value = "stock/{id}", method = RequestMethod.GET)
    public ResponseEntity<Stock> getStockInformation(@PathVariable("id") String productId) {
        LOGGER.info("Fetching stock information with id {}", productId);
        return new ResponseEntity<>(webShopRestService.getStockInformation(productId), HttpStatus.OK);
    }

}
