package pygitta.beadando01.domain;

import java.util.Date;

public class Stock {

    private String productId;

    private int piece;

    private double price;

    private Date date;

    public Stock(String productId, int piece, double price, Date date) {
        this.productId = productId;
        this.piece = piece;
        this.price = price;
        this.date = date;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getPiece() {
        return piece;
    }

    public void setPiece(int piece) {
        this.piece = piece;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
