package pygitta.beadando01.service;


import pygitta.beadando01.domain.Sale;

public interface WebShopSoapService {

    void sellProduct(Sale sale);
}
