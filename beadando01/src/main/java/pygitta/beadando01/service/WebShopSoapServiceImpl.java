package pygitta.beadando01.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import pygitta.beadando01.dao.WebShopDao;
import pygitta.beadando01.domain.Sale;
import pygitta.beadando01.exception.OutOfStockException;

@Service
public class WebShopSoapServiceImpl implements WebShopSoapService {

    private WebShopDao webShopDao;

    @Autowired
    public WebShopSoapServiceImpl(WebShopDao webShopDao) {
        this.webShopDao = webShopDao;
    }

    @Override
    public void sellProduct(Sale sale) {
        if (webShopDao.getStockInformation(sale.getProductId()).getPiece() > 0) {
            webShopDao.insertNewSaleData(sale);
        } else {
            throw new OutOfStockException("The product is out of stock.");
        }
    }
}
